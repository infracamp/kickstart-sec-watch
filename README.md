# kickstart-sec-watch

CI Job checking kickstart-flavor containers for security updates


## See updates

Download

```
https://gitlab.com/infracamp/kickstart-sec-watch/-/jobs/artifacts/master/file/security/security.json?job=test
```
Raw download
```
https://gitlab.com/infracamp/kickstart-sec-watch/-/jobs/artifacts/master/file/security/security.json?job=test
```
